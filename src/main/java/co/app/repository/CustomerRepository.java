package co.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.app.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
