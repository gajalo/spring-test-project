package co.app.conf;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@org.springframework.context.annotation.Configuration
@ComponentScan("co.app")
@EnableJpaRepositories("co.app.repository")
@EntityScan("co.app.model")
public class Configuration {

}
