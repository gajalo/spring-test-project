package co.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.List;
import co.app.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import co.app.repository.CustomerRepository;

@RestController
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepository;
	
	@PostMapping("/customer")
    public ResponseEntity<Void> saveCustomer(@RequestBody Customer customer) {
		customerRepository.save(customer);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
	
	@GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomers() {        
		List<Customer> customers  = customerRepository.findAll();
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.CREATED);
    }
}
